require 'time'
class Timer
  def initialize(sec=0, zone='PST')
    @@seconds = sec
    @@timezone = zone
  end

  def seconds=(int)
    @@seconds = int
  end

  def time_string
    ##HH:MM:SS
    Time.at(@@seconds).utc.strftime("%H:%M:%S")
  end

  def seconds
    @@seconds
  end

end


Timer.new()
