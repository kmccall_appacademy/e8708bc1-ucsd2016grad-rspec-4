class Temperature
  def initialize(opts)
    @@f = opts[:f]
    @@c = opts[:c]
  end

  def set_celsius(c)
    @@c = c
    @@f = nil
  end

  def in_fahrenheit
    if @@f
      @@f
    else
      self.class.ctof(@@c)
    end
  end

  def in_celsius
    if @@c
      @@c
    else
      self.class.ftoc(@@f)
    end
  end

  def self.ftoc(f)
    (f - 32.0) * 5.0 / 9.0
  end

  def self.ctof(c)
    c * (9.0 / 5.0) + 32.0
  end

  def self.from_celsius(c)
    Temperature.new(:c => c)
  end

  def self.from_fahrenheit(f)
    Temperature.new(:f => f)
  end
end

class Fahrenheit < Temperature
  def initialize(f)
    @@f = f
  end
end

class Celsius < Temperature
  def initialize(c)
    @@c = c
  end
end
# Instance Methods
# => constructors (initialize - called when using ClassName.new)
# => getters (access instance vars)
# => setters (set instance vars)
#   => something= (ClassName.something = x)
#   => set_something(x)

# Class Methods
# => factory method (returns an instance of the class)
# => utility method (method relating to the class that does something
# =>             useful but doesn't relate to an particular instance)

Temperature.new(:c => 5)
