class Book
  def title=(str)
    words = str.split
    words.each_with_index do |word, i|
      if i == 0 || !junction(word)
        word.capitalize!
      end
    end
    @@tpoop = words.join(" ")
  end

  def title
    @@tpoop
  end
end

def junction(word)
  ["an", "a", "the", "in", "of", "and"].include? (word)
end


# book = Book.new
#
# book.title = 'asdf'
#
# book.title ("Asdf")
