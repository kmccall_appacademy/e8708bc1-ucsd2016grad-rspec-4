class Dictionary
  def initialize
    @@hash = {}
  end

  def entries
    @@hash
  end

  def add(entry)
    if entry.is_a?(String)
      @@hash[entry] = nil
    else
      @@hash.merge! (entry)
    end
  end

  def keywords
    @@hash.keys.sort
  end

  def include?(key)
    @@hash.keys.include?(key)
  end

  def find(key)
    @@hash.select do |k, v|
      k.include? (key)
    end
  end

  def printable
    str = ""
    #[key] "value"\n
    @@hash.sort.each do |k, v|
      str += "[#{k}] \"#{v}\"\n"
    end
    str[0..-2]
  end
end
